import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'
import {Navbar} from 'react-bootstrap'
import useReactRouter from 'use-react-router'

import styled from 'styled-components'

import Login from './pages/login/Login'

// Course
import CourseList from './pages/courseList/CourseList'
import CourseAdd from './pages/courseAdd/CourseAdd'
import CourseDetail from './pages/courseDetail/CourseDetail'
import CourseEdit from './pages/courseEdit/CourseEdit'
import CourseDocList from './pages/courseDocList/CourseDocList'

// Register
import RegistrationList from './pages/registrationList/RegistrationList'

// resgisteredCouresList
import RegisteredCourseList from './pages/registeredCourseList/RegisteredCourseList'
import RegisteredCourseDetail from './pages/registeredCourseDetail/RegisteredCourseDetail'
import RegisteredCourseDocList from './pages/registeredCourseDocList/RegisteredCourseDocList'

// CustomSideNav
import {CustomSideNav} from './common'

const Main = styled.main`
  /* position: relative; */
  overflow: hidden;
  transition: all .15s;
  padding: 0 20px;
  margin-left: ${props => (props.expanded ? 240 : 64)}px;
`
function Routes () {
  return (
    <Router>
      <Switch>
        {/* Before login routes */}
        <Route exact path='/' component={Login} />

        {/* After login routes (has SideNav and NavBar) */}
        <Route
          render={({location, history}) =>
            <React.Fragment>
              {/* sidenav */}
              <CustomSideNav location={location} history={history} />

              <Main>
                {/* navbar */}
                <Navbar fixed='top' style={{height: 50}} bg='white' />

                {/* Contents */}
                <div
                  style={{
                    marginTop: 60,
                    backgroundColor: '#eee',
                    minHeight: '100vh'
                  }}
                >
                  <Route
                    path='/course-list'
                    exact
                    component={props => <CourseList />}
                  />
                  <Route
                    path='/course-add'
                    component={props => <CourseAdd />}
                  />
                  <Route
                    path='/course-detail'
                    component={props => <CourseDetail />}
                  />
                  <Route
                    path='/course-edit'
                    component={props => <CourseEdit />}
                  />
                  <Route
                    path='/course-doc-list'
                    component={props => <CourseDocList />}
                  />

                  {/* registration routes */}
                  <Route
                    path='/registration-list'
                    component={props => <RegistrationList />}
                  />

                  {/* registered course */}
                  <Route
                    path='/registered-course-list'
                    component={props => <RegisteredCourseList />}
                  />
                  <Route
                    path='/registered-course-detail'
                    component={props => <RegisteredCourseDetail />}
                  />
                  <Route
                    path='/registered-course-doc-list'
                    component={props => <RegisteredCourseDocList />}
                  />
                </div>
              </Main>
            </React.Fragment>}
        />
      </Switch>
    </Router>
  )
}

export default Routes
