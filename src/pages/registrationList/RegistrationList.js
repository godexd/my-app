import React, { useState } from 'react'
import useReactRouter from 'use-react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import Consts from '../../consts'
import {
  CustomContainer,
  SearchBar,
  Title,
  CustomButton,
  TableHeader,
  TableCell
} from '../../common'
import { useQuery, useMutation } from '@apollo/react-hooks';
import { REGISTRATIONS } from '../../apollo/registration'

function RegistrationList() {
  const { history, location, match } = useReactRouter()
  const apolloData = useQuery(REGISTRATIONS);
  const { loading, error } = apolloData
  const registrationData = apolloData.data && apolloData.data.registrations ? apolloData.data.registrations : []
  console.log(registrationData)
  // States
  const [showSearchView, setShowSearchView] = useState(false)

  // Set states
  const _handleSearchViewClose = () => setShowSearchView(false)
  const _handleSearchViewShow = () => setShowSearchView(true)

  const _studentDetail = (event) => {
    history.push('/register-detail', event)
  }

  const _studentEdit = (event) => {
    history.push('/register-edit', event)
  }

  const _studentAdd = () => {
    history.push('/register-add')
  }

  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item href='' onClick={() => history.push('/register-list')}>
          ລົງທະບຽນວິຊາ
        </Breadcrumb.Item>
        <Breadcrumb.Item active>ລາຍການລົງທະບຽນທັງຫມົດ</Breadcrumb.Item>
      </Breadcrumb>

      <CustomContainer>
        <Title text={'ລາຍການລົງທະບຽນທັງຫມົດ'} />
        <div style={{ textAlign: 'right' }}>
          <CustomButton
            confirm
            addIcon
            title='ລົງທະບຽນ'
            onClick={() => _studentAdd()}
          />
        </div>

        {/* custom search button */}
        <SearchBar
          title='ຄະນະວິທະຍາສາດທໍາມະຊາດ,ພາກວິຊາວິທະຍາສາດຄອມພິວເຕີ,ປີຮຽນທີ່1'
          onClick={() => _handleSearchViewShow()}
        />

        {/* ລາຍຊື່ນັກຮຽນ */}
        <div
          style={{
            marginTop: 24,
            marginBottom: 8,
            fontSize: 16,
            color: Consts.FONT_COLOR_SECONDARY
          }}
        >
          ທັງຫມົດ 28
        </div>

        {/* Table list */}
        <div>
          <table border='1' bordercolor='#fff' style={{ width: '100%' }}>
            <thead>
              <TableHeader>
                <th>ລຳດັບ</th>
                <th>ລະຫັດນັກຮຽນ</th>
                <th>ຊື່ນັກຮຽນ</th>
                <th>ລະຫັດວິຊາ</th>
                <th>ຊື່ວິຊາ</th>
                <th>ວັນທີລົງທະບຽນ</th>
                <th>ຈັດການ</th>
              </TableHeader>
            </thead>
            <tbody>
              {registrationData.length > 0 && registrationData.map((x, index) => {
                return (
                  <tr
                    style={{
                      borderBottom: '2px solid #ffff',
                      textAlign: 'center'
                    }}
                    key={index}
                  >
                    <TableCell>
                      {index + 1}
                    </TableCell>
                    <TableCell>{x.userId}</TableCell>
                    <TableCell>{x.firstname} {x.lastname}</TableCell>
                    <TableCell>ວິທະຍາສາດທຳມະຊາດ</TableCell>
                    <TableCell>ວິທະຍາສາດຄອມພິວເຕີ</TableCell>
                    <TableCell>1</TableCell>
                    <TableCell>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          justifyContent: 'space-around'
                        }}
                      >
                        <div onClick={() => _studentEdit(x)} style={{ cursor: 'pointer' }}>
                          <FontAwesomeIcon
                            icon={['fas', 'edit']}
                            color='#5d5d5d'
                          />{' '}
                        </div>
                        <div
                          onClick={() => _studentDetail(x)}
                          style={{ cursor: 'pointer' }}
                        >
                          <FontAwesomeIcon
                            icon={['fas', 'eye']}
                            color='#5d5d5d'
                          />{' '}
                        </div>
                      </div>
                    </TableCell>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </CustomContainer>


    </div>
  )
}

export default RegistrationList
