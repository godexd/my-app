import React, {Component} from 'react'
import {Form, Row, Col} from 'react-bootstrap'
import useReactRouter from 'use-react-router'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import CustomButton from '../../common/CustomButton'

function Login () {
  const {history, location, match} = useReactRouter()

  const _handleSubmit = event => {
    history.push('/course-list')
  }

  return (
    <div>
      <div className=''>
        <div style={{height: 30}} />
        <div className='text-center'>
          <img style={{width: 200, height: 180}} src='/assets/logo-login.png' />
        </div>
        <div style={{height: 15}} />
        <div className='d-flex justify-content-center'>
          <div
            style={{
              backgroundColor: '#fff',
              width: '50vw',
              padding: 30,
              paddingLeft: 80,
              paddingRight: 80
            }}
          >
            <h2>ກະລຸນາລ໊ອກອິນເພື່ອເຂົ້າສູ່ລະບົບ</h2>
            <Form>
              <Form.Group as={Row} controlId='formPlaintextEmail'>
                <Form.Label column sm='2'>
                  <FontAwesomeIcon
                    icon={['fas', 'user']}
                    size='2x'
                    color='#5d5d5d'
                  />
                </Form.Label>
                <Col sm='10'>
                  <Form.Control type='text' placeholder='ໄອດີ' />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId='formPlaintextPassword'>
                <Form.Label column sm='2'>
                  <FontAwesomeIcon
                    icon={['fas', 'lock']}
                    size='2x'
                    color='#5d5d5d'
                  />
                </Form.Label>
                <Col sm='10'>
                  <Form.Control type='password' placeholder='ລະຫັດຜ່ານ' />
                </Col>
              </Form.Group>
              <div style={{height: 30}} />
              <CustomButton
                confirm
                width='100%'
                title='ລ໊ອກອິນ'
                onClick={() => _handleSubmit()}
              />
            </Form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Login
