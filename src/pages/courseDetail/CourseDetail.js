import React, {useState, useCallback} from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import useReactRouter from 'use-react-router'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import Consts from '../../consts'
import CourseDeleteConfirm from './CourseDeleteConfirm'
import {CustomContainer, SearchBar, Title, CustomButton} from '../../common'

function CourseDetail () {
  const {history, location, match} = useReactRouter()

  // States
  const [showDeleteConfirmView, setShowDeleteConfirmView] = useState(false)

  // Set states
  const _handleDeleteConfirmViewClose = () => setShowDeleteConfirmView(false)
  const _handleDeleteConfirmViewShow = () => setShowDeleteConfirmView(true)

  const _viewDoc = () => {
    history.push('/course-doc-list')
  }

  const _edit = () => {
    history.push('/course-edit')
  }

  const _delete = () => {
    _handleDeleteConfirmViewShow()
  }

  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => history.push('/course-list')}>
          ຈັດການວິຊາ
        </Breadcrumb.Item>
        <Breadcrumb.Item onClick={() => history.push('/course-list')}>
          ວິຊາທັງຫມົດ
        </Breadcrumb.Item>
        <Breadcrumb.Item active>ລາຍລະອຽດວິຊາ</Breadcrumb.Item>
      </Breadcrumb>

      <CustomContainer>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <Title text='ລາຍລະອຽດວິຊາ' />

          {/* Button group */}
          <div>
            {/* ເອກະສານບົດສອນ */}
            <button
              style={{
                backgroundColor: '#fff',
                color: Consts.FONT_COLOR_SECONDARY,
                width: 140,
                height: 30,
                border: '1px solid #ddd',
                outline: 'none',
                marginRight: 5
              }}
              onClick={() => _viewDoc()}
            >
              ເອກະສານບົດສອນ
            </button>

            {/* ແກ້ໃຂ */}
            <button
              style={{
                backgroundColor: '#6f6f6f',
                color: '#fff',
                width: 80,
                height: 30,
                border: '1px solid #ddd',
                outline: 'none',
                marginRight: 5
              }}
              onClick={() => _edit()}
            >
              <FontAwesomeIcon icon='pencil' style={{fontSize: 16}} /> ແກ້ໃຂ
            </button>

            {/* ລຶບ */}
            <button
              style={{
                backgroundColor: '#6f6f6f',
                color: '#fff',
                width: 80,
                height: 30,
                border: '1px solid #ddd',
                outline: 'none'
              }}
              onClick={() => _delete()}
            >
              <i className='fa fa-trash' /> ລຶບ
            </button>
          </div>
        </div>

        <div
          style={{
            width: 500,
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: 20,
            paddingBottom: 80
          }}
        >
          {/* ------ detail box ------ */}
          <div
            style={{
              border: '1px solid #ddd',
              width: 500,
              padding: 20,
              fontSize: 14,
              paddingLeft: 80
            }}
          >
            <Row>
              <Col>ຊື່ວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                ຖານຂໍ້ມູນ1
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ລະຫັດວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                DB101
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ຈໍານວນຫນ່ວຍກິດ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                2
              </Col>
            </Row>
          </div>

          {/* -------- ຄະນະແລະພາກວິຊາ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ຄະນະແລະພາກວິຊາ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ຄະນະ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
              <Row>
                <Col>ພາກວິຊາ</Col>
                <Col>DB101</Col>
              </Row>
            </div>
          </div>

          {/* -------- ປີຮຽນແລະພາກຮຽນ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ປີຮຽນແລະພາກຮຽນ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ປີຮຽນ</Col>
                <Col>2</Col>
              </Row>
              <Row>
                <Col>ພາກຮຽນ</Col>
                <Col>2</Col>
              </Row>
            </div>
          </div>

          {/* -------- ຕາຕະລາງມື້ສອນ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ຕາຕະລາງມື້ສອນ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ວັນ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
              <Row>
                <Col>ຊົ່ວໂມງ</Col>
                <Col>DB101</Col>
              </Row>
            </div>
          </div>

          {/* -------- ອາຈານສິດສອນ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ອາຈານສິດສອນ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ຊື່ອາຈານ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
            </div>
          </div>

          {/* -------- ຄໍາອະທິບາຍ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ຄໍາອະທິບາຍ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ເນື້ອໃນຂອງວິຊາ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
            </div>
          </div>

          {/* -------- ອັບໂຫລດ Syllabus -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ອັບໂຫລດ Syllabus</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ອັບໂຫລດໄຟລ (PDF)</Col>
                <Col>db101.pdf</Col>
              </Row>
            </div>
          </div>
        </div>

        {/* ------- Delete Modal ------ */}
        {/* DeleteConfirm Modal */}
        <CourseDeleteConfirm
          showDeleteConfirmView={showDeleteConfirmView}
          _handleDeleteConfirmViewClose={_handleDeleteConfirmViewClose}
        />
      </CustomContainer>
    </div>
  )
}

export default CourseDetail
