import React, {useState} from 'react'
import useReactRouter from 'use-react-router'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import Consts from '../../consts'
import {COURSES} from '../../apollo/course'
import {
  CustomContainer,
  SearchBar,
  Title,
  CustomButton,
  TableHeader,
  TableCell
} from '../../common'
import {useQuery, useMutation} from '@apollo/react-hooks'
import RegisteredCourseDeleteConfirm from './RegisteredCourseDeleteConfirm'

function RegisteredCourseList () {
  const {history, location, match} = useReactRouter()
  const apolloData = useQuery(COURSES)
  const {loading, error} = apolloData
  const courseData =
    apolloData.data && apolloData.data.courses ? apolloData.data.courses : []

  // States
  const [showDeleteConfirmView, setShowDeleteConfirmView] = useState(false)

  // Set states
  const _handleDeleteConfirmViewClose = () => setShowDeleteConfirmView(false)
  const _handleDeleteConfirmViewShow = () => setShowDeleteConfirmView(true)

  const _courseDetail = event => {
    history.push('/registered-course-detail')
  }

  const _courseRemove = () => {
    // TODO: show popup delete view
    _handleDeleteConfirmViewShow()
  }

  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item active onClick={() => history.push('/course-list')}>
          ວິຊາທີ່ລົງທະບຽນທັງຫມົດ
        </Breadcrumb.Item>
      </Breadcrumb>

      <CustomContainer>
        <Title text={'ວິຊາທີ່ລົງທະບຽນທັງຫມົດ'} />

        {/* ວິຊາທັງຫມົດ */}
        <div
          style={{
            marginTop: 24,
            marginBottom: 8,
            fontSize: 16,
            color: Consts.FONT_COLOR_SECONDARY
          }}
        >
          ທັງຫມົດ 28 ວິຊາ
        </div>

        {/* Table list */}
        <div>
          <table border='1' bordercolor='#fff' style={{width: '100%'}}>
            <thead>
              <TableHeader>
                <th>ລຳດັບ</th>
                <th>ລະຫັດວິຊາ</th>
                <th>ຊື່ວິຊາ</th>
                <th>ວັນ</th>
                <th>ຊົ່ວໂມງ</th>
                <th>ວັນທີລົງທະບຽນ</th>
                <th>ຈັດການ</th>
              </TableHeader>
            </thead>
            <tbody>
              {courseData.length > 0 &&
                courseData.map((x, index) => {
                  return (
                    <tr
                      style={{
                        borderBottom: '2px solid #ffff',
                        textAlign: 'center'
                      }}
                      key={index}
                    >
                      <TableCell>
                        {index + 1}
                      </TableCell>
                      <TableCell>
                        {x.courseCode}
                      </TableCell>
                      <TableCell>
                        {x.title}
                      </TableCell>
                      <TableCell>Mon</TableCell>
                      <TableCell>4</TableCell>
                      <TableCell>4/29/2019</TableCell>
                      <TableCell style={{width: 200}}>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-around',
                            width: 200
                          }}
                        >
                          <button
                            onClick={_courseDetail}
                            style={{
                              width: 60,
                              height: 30,
                              borderRadius: 3,
                              border: '1px solid #ddd',
                              outline: 'none'
                            }}
                          >
                            <FontAwesomeIcon
                              icon={['fa', 'eye']}
                              color='#5d5d5d'
                            />
                          </button>

                          <button
                            onClick={_courseRemove}
                            style={{
                              width: 80,
                              height: 30,
                              borderRadius: 3,
                              border: '1px solid #ddd',
                              outline: 'none'
                            }}
                          >
                            ຍົກເລີກ <i className='fa fa-times-circle' />
                          </button>
                        </div>
                      </TableCell>
                    </tr>
                  )
                })}
            </tbody>
          </table>
        </div>

        {/* ------- Delete Modal ------ */}
        {/* DeleteConfirm Modal */}
        <RegisteredCourseDeleteConfirm
          showDeleteConfirmView={showDeleteConfirmView}
          _handleDeleteConfirmViewClose={_handleDeleteConfirmViewClose}
        />
      </CustomContainer>
    </div>
  )
}

export default RegisteredCourseList
