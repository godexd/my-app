import React, {useCallback} from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import Consts from '../../consts'

const RegisteredCourseDeleteConfirm = ({
  showDeleteConfirmView,
  _handleDeleteConfirmViewClose
}) => {
  return (
    <Modal
      show={showDeleteConfirmView}
      onHide={_handleDeleteConfirmViewClose}
      size='md'
    >
      <Modal.Body style={{marginLeft: 50, marginRight: 50, padding: 50}}>
        <p className='text-center' style={{fontWeight: 'bold'}}>
          ຍົກເລີກການລົງທະບຽນ?
        </p>

        <p className='text-center'>ວິຊາຖານຂໍ້ມູນ</p>

        <div style={{height: 20}} />
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <div style={{padding: 15}}>
            <Button
              onClick={_handleDeleteConfirmViewClose}
              style={{
                width: 120,
                backgroundColor: '#fff',
                color: '#6f6f6f',
                borderColor: '#6f6f6f'
              }}
            >
              ບໍ່ຍົກເລີກ
            </Button>
          </div>
          <div style={{padding: 15}}>
            <Button
              style={{
                width: 120,
                backgroundColor: Consts.SECONDARY_COLOR,
                color: '#fff',
                borderColor: Consts.SECONDARY_COLOR
              }}
            >
              ຍົກເລີກ
            </Button>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default RegisteredCourseDeleteConfirm
