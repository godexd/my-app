import React, { useState } from 'react'
import useReactRouter from 'use-react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import CourseSearch from './CourseSearch'
import Consts from '../../consts'
import {
  CustomContainer,
  SearchBar,
  Title,
  CustomButton,
  TableHeader,
  TableCell
} from '../../common'
import { useQuery, useMutation } from '@apollo/react-hooks';
import { COURSES } from '../../apollo/course'

function CourseList() {
  const { history, location, match } = useReactRouter()
  const apolloData = useQuery(COURSES);
  const { loading, error } = apolloData
  const courseData = apolloData.data && apolloData.data.courses ? apolloData.data.courses : []
  console.log(apolloData)
  console.log(courseData)

  // States
  const [showSearchView, setShowSearchView] = useState(false)

  // Set states
  const _handleSearchViewClose = () => setShowSearchView(false)
  const _handleSearchViewShow = () => setShowSearchView(true)

  const _courseDetail = event => {
    history.push('/course-detail')
  }

  const _courseEdit = () => {
    history.push('/course-edit')
  }

  const _courseAdd = () => {
    history.push('/course-add')
  }

  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item href='' onClick={() => history.push('/course-list')}>
          ຈັດການວິຊາ
        </Breadcrumb.Item>
        <Breadcrumb.Item active>ວິຊາທັງຫມົດ</Breadcrumb.Item>
      </Breadcrumb>

      <CustomContainer>
        <Title text={'ວິຊາທັງຫມົດ'} />
        <div style={{ textAlign: 'right' }}>
          <CustomButton
            confirm
            addIcon
            title='ເພີ່ມວິຊາ'
            onClick={() => _courseAdd()}
          />
        </div>

        {/* custom search button */}
        <SearchBar
          title='ຄະນະວິທະຍາສາດທໍາມະຊາດ,ພາກວິຊາວິທະຍາສາດຄອມພິວເຕີ,ປີຮຽນທີ່1'
          onClick={() => _handleSearchViewShow()}
        />

        {/* ວິຊາທັງຫມົດ */}
        <div
          style={{
            marginTop: 24,
            marginBottom: 8,
            fontSize: 16,
            color: Consts.FONT_COLOR_SECONDARY
          }}
        >
          ທັງຫມົດ 28 ວິຊາ
        </div>

        {/* Table list */}
        <div>
          <table border='1' bordercolor='#fff' style={{ width: '100%' }}>
            <thead>
              <TableHeader>
                <th>ລຳດັບ</th>
                <th>ລະຫັດວິຊາ</th>
                <th>ຊື່ວິຊາ</th>
                <th>ພາກຮຽນ</th>
                <th>ວັນ</th>
                <th>ຊົ່ວໂມງ</th>
                <th>ອາຈານ</th>
                <th>ຈັດການ</th>
              </TableHeader>
            </thead>
            <tbody>
              {courseData.length>0 && courseData.map((x, index) => {
                return (
                  <tr
                    style={{
                      borderBottom: '2px solid #ffff',
                      textAlign: 'center'
                    }}
                    key={index}
                  >
                    <TableCell>
                      {index + 1}
                    </TableCell>
                    <TableCell>{x.courseCode}</TableCell>
                    <TableCell>{x.title}</TableCell>
                    <TableCell>2</TableCell>
                    <TableCell>Mon</TableCell>
                    <TableCell>4</TableCell>
                    <TableCell>King</TableCell>
                    <TableCell>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          justifyContent: 'space-around'
                        }}
                      >
                        <div onClick={_courseEdit} style={{ cursor: 'pointer' }}>
                          <FontAwesomeIcon
                            icon={['fas', 'edit']}
                            color='#5d5d5d'
                          />{' '}
                        </div>
                        <div
                          onClick={_courseDetail}
                          style={{ cursor: 'pointer' }}
                        >
                          <FontAwesomeIcon
                            icon={['fas', 'eye']}
                            color='#5d5d5d'
                          />{' '}
                        </div>
                      </div>
                    </TableCell>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </CustomContainer>

      {/* Search Modal */}
      <CourseSearch
        showSearchView={showSearchView}
        _handleSearchViewClose={_handleSearchViewClose}
      />
    </div>
  )
}

export default CourseList
