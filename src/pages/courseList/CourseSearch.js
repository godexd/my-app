import React from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import {
  CustomContainer,
  SearchBar,
  Title,
  CustomButton,
  TableHeader,
  TableCell
} from '../../common'

const CourseSearch = ({showSearchView, _handleSearchViewClose}) => {
  return (
    <Modal show={showSearchView} onHide={_handleSearchViewClose} size='lg'>
      <Modal.Title style={{textAlign: 'center', paddingTop: 20}}>
        ຄົ້ນຫາວິຊາ
      </Modal.Title>

      <Modal.Body style={{marginLeft: 50, marginRight: 50, padding: 50}}>
        <Form.Group
          as={Row}
          controlId='formPlaintextEmail'
          style={{margin: 0, marginBottom: 10}}
        >
          <Form.Label column sm='4' className='text-left'>
            ຄະນະ
          </Form.Label>
          <Col sm='8'>
            <Form.Control as='select'>
              <option>ວິທະຍາສາດທໍາມະຊາດ</option>
              <option>ວິທະຍາສາດທໍາມະຊາດ</option>
              <option>ວິທະຍາສາດທໍາມະຊາດ</option>
              <option>ວິທະຍາສາດທໍາມະຊາດ</option>
              <option>ວິທະຍາສາດທໍາມະຊາດ</option>
            </Form.Control>
          </Col>
        </Form.Group>

        <Form.Group
          as={Row}
          controlId='formPlaintextEmail'
          style={{margin: 0, marginBottom: 10}}
        >
          <Form.Label column sm='4' className='text-left'>
            ພາກວິຊາ
          </Form.Label>
          <Col sm='8'>
            <Form.Control as='select'>
              <option>ວິທະຍາສາດຄອມພິວເຕີ</option>
              <option>ວິທະຍາສາດຄອມພິວເຕີ</option>
            </Form.Control>
          </Col>
        </Form.Group>

        <Form.Group
          as={Row}
          controlId='formPlaintextEmail'
          style={{margin: 0, marginBottom: 10}}
        >
          <Form.Label column sm='4' className='text-left'>
            ປີຮຽນ
          </Form.Label>
          <Col sm='8'>
            <Form.Control as='select'>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </Form.Control>
          </Col>
        </Form.Group>

        <hr />

        <Form.Group
          as={Row}
          controlId='formPlaintextEmail'
          style={{margin: 0, marginBottom: 10}}
        >
          <Form.Label column sm='4' className='text-left'>
            ລະຫັດວິຊາ
          </Form.Label>
          <Col sm='8'>
            <Form.Control type='text' placeholder='ກະລຸນາປ້ອນ' />
          </Col>
        </Form.Group>

        <div style={{height: 20}} />
        <div className='row'>
          <div style={{padding: 15}} className='col'>
            <CustomButton
              confirm
              onClick={_handleSearchViewClose}
              width='100%'
              title='ຄົ້ນຫາ'
            />
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default CourseSearch
