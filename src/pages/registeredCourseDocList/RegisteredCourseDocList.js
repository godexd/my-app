import React, {useState} from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import useReactRouter from 'use-react-router'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import Consts from '../../consts'
// import CourseDocUpload from './CourseDocUpload'
// import CourseDocEdit from './CourseDocEdit'

import {
  CustomContainer,
  SearchBar,
  Title,
  CustomButton,
  TableHeader,
  TableCell
} from '../../common'

const RegisteredCourseDocList = () => {
  const {history, location, match} = useReactRouter()

  // State
  const [courseDocUploadModal, setCourseDocUploadModal] = useState(false)
  const [courseDocEditModal, setCourseDocEditModal] = useState(false)
  const [
    courseDocDeleteConfirmModal,
    setCourseDocDeleteConfirmModal
  ] = useState(false)
  // Set states
  const _handlCourseDocUploadModalClose = () => setCourseDocUploadModal(false)
  const _handlCourseDocUploadModalShow = () => setCourseDocUploadModal(true)
  const _handlCourseDocEditModalClose = () => setCourseDocEditModal(false)
  const _handlCourseDocEditModalShow = () => setCourseDocEditModal(true)
  const _handlCourseDocDeleteConfirmModalClose = () =>
    setCourseDocDeleteConfirmModal(false)
  const _handlCourseDocDeleteConfirmModalShow = () =>
    setCourseDocDeleteConfirmModal(true)

  const _courseDetail = () => {
    history.push('/registered-course-detail')
  }

  const _downloadFile = () => {
    // TODO: download file
  }

  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item
          onClick={() => history.push('/registered-course-list')}
        >
          ວິຊາທີ່ລົງທະບຽນທັງຫມົດ
        </Breadcrumb.Item>
        <Breadcrumb.Item href='/registered-course-detail'>
          ລາຍລະອຽດວິຊາ
        </Breadcrumb.Item>
        <Breadcrumb.Item active>ເອກະສານວິຊາ</Breadcrumb.Item>
      </Breadcrumb>

      {/* Container */}
      <CustomContainer>
        {/* --------- Title and Button groups ----------- */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <Title text='ເອກະສານວິຊາ' />

          {/* Button group */}
          <div>
            {/* ລາຍລະອຽດວິຊາ */}
            <button
              style={{
                backgroundColor: '#fff',
                color: Consts.FONT_COLOR_SECONDARY,
                width: 160,
                height: 35,
                border: `1px solid ${Consts.FONT_COLOR_SECONDARY}`,
                outline: 'none',
                borderRadius: 0,
                marginRight: 20
              }}
              onClick={() => _courseDetail()}
            >
              ລາຍລະອຽດວິຊາ
            </button>
          </div>
        </div>
        {/* -------- ຂໍ້ມູນວິຊາ ----------- */}
        <div style={{marginTop: 10}}>
          {/* ------ detail box ------ */}
          <div
            style={{
              border: '1px solid #ddd',
              width: '60%',
              padding: 20,
              fontSize: 14,
              marginRight: 'auto',
              marginLeft: 'auto',
              marginTop: 20,
              paddingLeft: 80
            }}
          >
            <Row>
              <Col>ຊື່ວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                ຖານຂໍ້ມູນ1
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ລະຫັດວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                DB101
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ຈໍານວນຫນ່ວຍກິດ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                2
              </Col>
            </Row>
          </div>
        </div>

        <div style={{height: 80}} />

        {/* ---------- table --------- */}
        <div>
          <table border='1' bordercolor='#fff' style={{width: '100%'}}>
            <thead>
              <TableHeader>
                <th>ລຳດັບ</th>
                <th>ຫົວຂໍ້</th>
                <th>ໄຟລ</th>
                <th>ອັບເດດ</th>
                <th>ຈັດການ</th>
              </TableHeader>
            </thead>
            <tbody>
              {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((data, index) => {
                return (
                  <tr
                    style={{
                      borderBottom: '2px solid #ffff',
                      textAlign: 'center'
                    }}
                    key={index}
                  >
                    <TableCell>
                      {index + 1}
                    </TableCell>
                    <TableCell>SQL Introduction</TableCell>
                    <TableCell style={{textDecoration: 'underline'}}>
                      DB101.pdf
                    </TableCell>
                    <TableCell>2019/08/23 10:23</TableCell>
                    <TableCell>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          justifyContent: 'space-around'
                        }}
                      >
                        <button
                          onClick={() => _downloadFile()}
                          style={{
                            width: 80,
                            height: 30,
                            borderRadius: 3,
                            border: '1px solid #ddd',
                            outline: 'none'
                          }}
                        >
                          <i className='fa fa-download' />
                        </button>
                      </div>
                    </TableCell>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </CustomContainer>
    </div>
  )
}

export default RegisteredCourseDocList
