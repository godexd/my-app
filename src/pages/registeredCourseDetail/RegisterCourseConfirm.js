import React, {useCallback} from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import Consts from '../../consts'

const RegisterCourseConfirm = ({
  showRegisterConfirmView,
  _handleRegisterConfirmViewClose
}) => {
  return (
    <Modal
      show={showRegisterConfirmView}
      onHide={_handleRegisterConfirmViewClose}
      size='md'
    >
      <Modal.Body style={{marginLeft: 50, marginRight: 50, padding: 50}}>
        <p className='text-center' style={{fontWeight: 'bold'}}>
          ຢືນຢັນການລົງທະບຽນ?
        </p>

        <p className='text-center'>ວິຊາຖານຂໍ້ມູນ</p>

        <div style={{height: 20}} />
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <div style={{padding: 15}}>
            <Button
              onClick={_handleRegisterConfirmViewClose}
              style={{
                width: 120,
                backgroundColor: '#fff',
                color: '#6f6f6f',
                borderColor: '#6f6f6f'
              }}
            >
              ຍົກເລີກ
            </Button>
          </div>
          <div style={{padding: 15}}>
            <Button
              style={{
                width: 120,
                backgroundColor: Consts.SECONDARY_COLOR,
                color: '#fff',
                borderColor: Consts.SECONDARY_COLOR
              }}
            >
              ຕົກລົງ
            </Button>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default RegisterCourseConfirm
