import React, {useState, useCallback} from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import useReactRouter from 'use-react-router'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import Consts from '../../consts'
import RegisterCourseConfirm from './RegisterCourseConfirm'
import {CustomContainer, SearchBar, Title, CustomButton} from '../../common'

function RegisteredCourseDetail () {
  const {history, location, match} = useReactRouter()

  // States
  const [showRegisterConfirmView, setShowRegisterConfirmView] = useState(false)

  // Set states
  const _handleRegisterConfirmViewClose = () =>
    setShowRegisterConfirmView(false)
  const _handleRegisterConfirmViewShow = () => setShowRegisterConfirmView(true)

  const _viewDoc = () => {
    history.push('/registered-course-doc-list')
  }

  const _register = () => {
    console.log('_register')
    _handleRegisterConfirmViewShow()
  }

  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item
          onClick={() => history.push('/registered-course-list')}
        >
          ວິຊາທີ່ລົງທະບຽນທັງຫມົດ
        </Breadcrumb.Item>
        <Breadcrumb.Item active>ລາຍລະອຽດວິຊາ</Breadcrumb.Item>
      </Breadcrumb>

      <CustomContainer>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <Title text='ລາຍລະອຽດວິຊາ' />

          {/* Button group */}
          <div>
            {/* ເອກະສານວິຊາ */}
            <button
              style={{
                backgroundColor: '#fff',
                color: Consts.FONT_COLOR_SECONDARY,
                width: 140,
                height: 40,
                border: '1px solid #ddd',
                outline: 'none',
                marginRight: 15
              }}
              onClick={() => _viewDoc()}
            >
              ເອກະສານວິຊາ
            </button>

            {/* ລົງທະບຽນ */}
            <CustomButton
              confirm
              onClick={() => _register()}
              title='ລົງທະບຽນ'
            />
          </div>
        </div>

        <div
          style={{
            width: 500,
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: 20,
            paddingBottom: 80
          }}
        >
          {/* ------ detail box ------ */}
          <div
            style={{
              border: '1px solid #ddd',
              width: 500,
              padding: 20,
              fontSize: 14,
              paddingLeft: 80
            }}
          >
            <Row>
              <Col>ຊື່ວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                ຖານຂໍ້ມູນ1
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ລະຫັດວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                DB101
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ຈໍານວນຫນ່ວຍກິດ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                2
              </Col>
            </Row>
          </div>

          {/* -------- ຄະນະແລະພາກວິຊາ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ຄະນະແລະພາກວິຊາ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ຄະນະ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
              <Row>
                <Col>ພາກວິຊາ</Col>
                <Col>DB101</Col>
              </Row>
            </div>
          </div>

          {/* -------- ປີຮຽນແລະພາກຮຽນ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ປີຮຽນແລະພາກຮຽນ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ປີຮຽນ</Col>
                <Col>2</Col>
              </Row>
              <Row>
                <Col>ພາກຮຽນ</Col>
                <Col>2</Col>
              </Row>
            </div>
          </div>

          {/* -------- ຕາຕະລາງມື້ສອນ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ຕາຕະລາງມື້ສອນ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ວັນ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
              <Row>
                <Col>ຊົ່ວໂມງ</Col>
                <Col>DB101</Col>
              </Row>
            </div>
          </div>

          {/* -------- ອາຈານສິດສອນ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ອາຈານສິດສອນ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ຊື່ອາຈານ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
            </div>
          </div>

          {/* -------- ຄໍາອະທິບາຍ -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ຄໍາອະທິບາຍ</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>ເນື້ອໃນຂອງວິຊາ</Col>
                <Col>ຖານຂໍ້ມູນ1</Col>
              </Row>
            </div>
          </div>

          {/* -------- ອັບໂຫລດ Syllabus -------- */}
          <div style={{padding: 20, paddingBottom: 0}}>
            <div>ອັບໂຫລດ Syllabus</div>
            <div style={{paddingLeft: 20, fontSize: 14}}>
              <Row>
                <Col>
                  ອັບໂຫລດໄຟລ <br />(.PDF)
                </Col>
                <Col style={{textDecoration: 'underline', fontSize: 20}}>
                  db101.pdf
                </Col>
              </Row>
            </div>
          </div>
        </div>

        {/* ------- Register Modal ------ */}
        {/* RegisterConfirm Modal */}
        <RegisterCourseConfirm
          showRegisterConfirmView={showRegisterConfirmView}
          _handleRegisterConfirmViewClose={_handleRegisterConfirmViewClose}
        />
      </CustomContainer>
    </div>
  )
}

export default RegisteredCourseDetail
