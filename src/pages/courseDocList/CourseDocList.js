import React, {useState} from 'react'
import {
  Breadcrumb,
  Modal,
  Button,
  Form,
  Row,
  Col,
  Table,
  InputGroup,
  FormControl
} from 'react-bootstrap'
import useReactRouter from 'use-react-router'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import Consts from '../../consts'
import CourseDocUpload from './CourseDocUpload'
import CourseDocEdit from './CourseDocEdit'
import CourseDocDeleteConfirm from './CourseDocDeleteConfirm'

import {
  CustomContainer,
  SearchBar,
  Title,
  CustomButton,
  TableHeader,
  TableCell
} from '../../common'

const CourseDocList = () => {
  const {history, location, match} = useReactRouter()

  // State
  const [courseDocUploadModal, setCourseDocUploadModal] = useState(false)
  const [courseDocEditModal, setCourseDocEditModal] = useState(false)
  const [
    courseDocDeleteConfirmModal,
    setCourseDocDeleteConfirmModal
  ] = useState(false)
  // Set states
  const _handlCourseDocUploadModalClose = () => setCourseDocUploadModal(false)
  const _handlCourseDocUploadModalShow = () => setCourseDocUploadModal(true)
  const _handlCourseDocEditModalClose = () => setCourseDocEditModal(false)
  const _handlCourseDocEditModalShow = () => setCourseDocEditModal(true)
  const _handlCourseDocDeleteConfirmModalClose = () =>
    setCourseDocDeleteConfirmModal(false)
  const _handlCourseDocDeleteConfirmModalShow = () =>
    setCourseDocDeleteConfirmModal(true)

  const _courseDetail = () => {
    history.push('/course-detail')
  }
  const _uploadFile = () => {
    _handlCourseDocUploadModalShow()
  }
  const _editFile = () => {
    _handlCourseDocEditModalShow()
  }
  const _removeFile = () => {
    _handlCourseDocDeleteConfirmModalShow()
  }
  return (
    <div>
      {/* Breadcrumb */}
      <Breadcrumb>
        <Breadcrumb.Item href='/course-list'>ຈັດການວິຊາ</Breadcrumb.Item>
        <Breadcrumb.Item href='/course-list'>ວິຊາທັງຫມົດ</Breadcrumb.Item>
        <Breadcrumb.Item active>ເອກະສານບົດສອນ</Breadcrumb.Item>
      </Breadcrumb>

      {/* Container */}
      <CustomContainer>
        {/* --------- Title and Button groups ----------- */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          <Title text='ເອກະສານບົດສອນ' />

          {/* Button group */}
          <div>
            {/* ລາຍລະອຽດວິຊາ */}
            <button
              style={{
                backgroundColor: '#fff',
                color: Consts.FONT_COLOR_SECONDARY,
                width: 160,
                height: 35,
                border: `1px solid ${Consts.FONT_COLOR_SECONDARY}`,
                outline: 'none',
                borderRadius: 0,
                marginRight: 20
              }}
              onClick={() => _courseDetail()}
            >
              ລາຍລະອຽດວິຊາ
            </button>

            {/* ອັບໂຫລດບົດສອນ */}
            <button
              style={{
                backgroundColor: Consts.SECONDARY_COLOR,
                color: '#fff',
                width: 160,
                height: 35,
                border: '1px solid #ddd',
                outline: 'none',
                borderRadius: 0
              }}
              onClick={() => _uploadFile()}
            >
              <FontAwesomeIcon icon='download' style={{fontSize: 16}} />{' '}
              ອັບໂຫລດບົດສອນ
            </button>
          </div>
        </div>

        {/* -------- ຂໍ້ມູນວິຊາ ----------- */}
        <div style={{marginTop: 10}}>
          <div>ຂໍ້ມູນວິຊາ</div>
          {/* ------ detail box ------ */}
          <div
            style={{
              border: '1px solid #ddd',
              width: '60%',
              padding: 20,
              fontSize: 14,
              marginRight: 'auto',
              marginLeft: 'auto',
              marginTop: -10,
              paddingLeft: 80
            }}
          >
            <Row>
              <Col>ຊື່ວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                ຖານຂໍ້ມູນ1
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ລະຫັດວິຊາ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                DB101
              </Col>
            </Row>
            <div style={{height: 10}} />
            <Row>
              <Col>ຈໍານວນຫນ່ວຍກິດ</Col>
              <Col
                style={{color: Consts.FONT_COLOR_PRIMARY, fontWeight: 'bold'}}
              >
                2
              </Col>
            </Row>
          </div>
        </div>

        {/* ------ ເອກະສານທີ່ຖືກອັບໂຫລດ -------- */}
        <div style={{marginTop: 20, marginBottom: 10}}>
          ເອກະສານທີ່ຖືກອັບໂຫລດ
        </div>

        {/* ---------- table --------- */}
        <div>
          <table border='1' bordercolor='#fff' style={{width: '100%'}}>
            <thead>
              <TableHeader>
                <th>ລຳດັບ</th>
                <th>ຫົວຂໍ້</th>
                <th>ໄຟລ</th>
                <th>ອັບເດດ</th>
                <th>ຈັດການ</th>
              </TableHeader>
            </thead>
            <tbody>
              {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((data, index) => {
                return (
                  <tr
                    style={{
                      borderBottom: '2px solid #ffff',
                      textAlign: 'center'
                    }}
                    key={index}
                  >
                    <TableCell>
                      {index + 1}
                    </TableCell>
                    <TableCell>SQL Introduction</TableCell>
                    <TableCell>DB101.pdf</TableCell>
                    <TableCell>2019/08/23 10:23</TableCell>
                    <TableCell>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          justifyContent: 'space-around'
                        }}
                      >
                        <div
                          onClick={() => _editFile()}
                          style={{cursor: 'pointer'}}
                        >
                          <FontAwesomeIcon
                            icon={['fas', 'edit']}
                            color='#5d5d5d'
                          />{' '}
                        </div>
                        <div
                          onClick={() => _removeFile()}
                          style={{cursor: 'pointer'}}
                        >
                          <FontAwesomeIcon
                            icon={['fas', 'trash']}
                            color='#5d5d5d'
                          />{' '}
                        </div>
                      </div>
                    </TableCell>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>

        {/* -------- Course doc upload modal ---------- */}
        <CourseDocUpload
          courseDocUploadModal={courseDocUploadModal}
          _handlCourseDocUploadModalClose={_handlCourseDocUploadModalClose}
        />

        {/* -------- Course doc edit modal ---------- */}
        <CourseDocEdit
          courseDocEditModal={courseDocEditModal}
          _handlCourseDocEditModalClose={_handlCourseDocEditModalClose}
        />

        {/* -------- Course delete confirm modal ---------- */}
        <CourseDocDeleteConfirm
          courseDocDeleteConfirmModal={courseDocDeleteConfirmModal}
          _handlCourseDocDeleteConfirmModalClose={
            _handlCourseDocDeleteConfirmModalClose
          }
        />
      </CustomContainer>
    </div>
  )
}

export default CourseDocList
