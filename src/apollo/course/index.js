import { gql } from 'apollo-boost';

export const COURSES = gql`
  query Courses($where: CourseWhereInput
$orderBy: CourseOrderByInput
$skip: Int
$after: String
$before: String
$first: Int
$last: Int) 
{
  courses(where: $where, orderBy: $orderBy,skip: $skip,after: $after,before: $before, first: $first, last: $last) {
    id
    courseCode
    title
    description
    note
    createdAt
    updatedAt
    }
  }
`;



export const CREATE_COURSE = gql`
  mutation CreateCourse($data: CourseCreateInput!) {
    createCourse(data: $data) {
      id
    }
  }
`;

export const UPDATE_COURSE = gql`
  mutation UpdateCourse($data: CourseUpdateInput!,$where: CourseWhereUniqueInput!) {
    updateCourse(data: $data , where:$where) {
      id
    }
  }
`;


export const DELETE_COURSE = gql`
  mutation DeleteCourse($where: CourseWhereUniqueInput!) {
    deleteCourse(where:$where) {
      id
    }
  }
`;
