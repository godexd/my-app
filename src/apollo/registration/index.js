
import { gql } from 'apollo-boost';

export const REGISTRATIONS = gql`
  query Registrations($where: RegistrationWhereInput
$orderBy: RegistrationOrderByInput
$skip: Int
$after: String
$before: String
$first: Int
$last: Int) 
{
  registrations(where: $where, orderBy: $orderBy,skip: $skip,after: $after,before: $before, first: $first, last: $last) {
    id
    student{
      id
    }
    course{
      id
    }
    note
    createdAt
    updatedAt
    }
  }
`;
