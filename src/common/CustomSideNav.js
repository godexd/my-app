import React, {useState} from 'react'
import '@trendmicro/react-sidenav/dist/react-sidenav.css'
import Consts from '../consts'

import SideNav, {
  Toggle,
  Nav,
  NavItem,
  NavIcon,
  NavText
} from '@trendmicro/react-sidenav'

function CustomSideNav ({location, history}) {
  const _goPage = path => {
    history.push(path)
  }
  return (
    <SideNav
      onSelect={selected => {
        const to = '/' + selected
        if (location.pathname !== to) {
          history.push(to)
        }
      }}
      style={{
        position: 'fixed',
        backgroundColor: Consts.PRIMARY_COLOR,
        zIndex: 10000
      }}
    >
      <SideNav.Toggle />
      <SideNav.Nav defaultSelected='course-list'>
        <NavItem eventKey='registration-list'>
          <NavIcon>
            <i
              className='fa fa-plus-square'
              aria-hidden='true'
              style={{fontSize: '1.75em'}}
            />
          </NavIcon>
          <NavText>ລົງທະບຽນວິຊາຮຽນ</NavText>
        </NavItem>
        <NavItem eventKey='registered-course-list'>
          <NavIcon>
            <i className='fa fa-bookmark' style={{fontSize: '1.75em'}} />
          </NavIcon>
          <NavText>ວິຊາທີ່ລົງທະບຽນ</NavText>
        </NavItem>
        <NavItem eventKey='timetable-list'>
          <NavIcon>
            <i className='fa fa-table' style={{fontSize: '1.75em'}} />
          </NavIcon>
          <NavText>ຕາຕະລາງຮຽນ</NavText>
        </NavItem>
        <NavItem eventKey='doc-list'>
          <NavIcon>
            <i className='fa fa-folder' style={{fontSize: '1.75em'}} />
          </NavIcon>
          <NavText>ເອກະສານ</NavText>
        </NavItem>
      </SideNav.Nav>
    </SideNav>
  )
}

export default CustomSideNav
