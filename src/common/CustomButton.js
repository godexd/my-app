import React, {Component} from 'react'
import Consts from '../consts'

function CustomButton ({title, onClick, confirm, addIcon, width}) {
  return (
    <button
      style={{
        backgroundColor: confirm ? Consts.SECONDARY_COLOR : '#fff',
        color: confirm ? '#fff' : '#000',
        width: width || 140,
        height: 40,
        border: confirm
          ? '1px solid ' + Consts.SECONDARY_COLOR
          : '1px solid #6f6f6f',
        outline: 'none'
      }}
      onClick={() => onClick()}
    >
      {addIcon && <i className='fa fa-plus' />} {title}
    </button>
  )
}

export default CustomButton
