import React, {Component} from 'react'

import Routes from './Routes'

import {ApolloProvider} from '@apollo/react-hooks'
import ApolloClient from 'apollo-boost'

import {library, dom} from '@fortawesome/fontawesome-svg-core'
import {fab} from '@fortawesome/free-brands-svg-icons'
import {fas} from '@fortawesome/free-solid-svg-icons'
library.add(fab, fas)
dom.watch()

const client = new ApolloClient({
  
})

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <ApolloProvider client={client}>
        <Routes />
      </ApolloProvider>
    )
  }
}

export default App
