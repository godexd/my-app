module.exports = {
  PRIMARY_COLOR: '#222939',
  SECONDARY_COLOR: '#4559ff',
  FONT_COLOR_PRIMARY: '#383838',
  FONT_COLOR_SECONDARY: '#6f6f6f',
  CONTAINER_PADDING: 24,
  CONTAINER_MARGIN_TOP: -10
}
